# Cypress

The Cypress docs will always have a better explanation than I can give, but here are some basic tips I can give you.

The simplest ways to select an element in the page are `cy.get()` and `cy.contains()`.

I highly recommend adding lots of comments to your tests as it's often very difficult to tell what element you're trying to select just from looking at the `get` command.

`cy.get()` will return an array of elements that match whatever arguments you give it in the order that they appear on the page. If your arguments were specific enough to only have one outcome, that's awesome. If not, then add `.eq(i)` where `i` is the position of the element you want in the array. This is where Cypress' "time travel" function is awesome - you can hover your `get` step and it will highlight all the elements it applies to, so you can figure out where it will be in the array.

Querying an element with `cy.get()` is very similar to jQuery. Hopefully it's as simple as something like `cy.get('div[class="classname"]')`, but it can get a bit more complex. If your element has a bunch of different class names, you can use `class*=` to find elements whose class _contains_ the string you specify.

`cy.contains()` is easier to use, but much more limited. It will yield the first element on the page that contains the text you give it. This is very useful for clicking unique links or buttons, but if you need to interact with an element that doesn't have a unique value, you'll have to use `cy.get`.

One trick I've learned is that you can actually use `cy.get('div:contains("string")')`, which will function basically the same as a `cy.contains()` except it will give you an array of all the elements it matches, instead of just selecting the first one it finds. Then you can use `.eq()` to select the right element.

Lastly, if you need to use the same exact steps many times across multiple tests, even in separate files, (for instance logging into the site) you can define custom functions in `cypress/support/commands.js`. You can set variables for this in `cypress.env.json` if you want, but that should only be necessary if you're testing a local build that has different login information 

**Happy Testing!**
